function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 8; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getMoney() {

    function validateEmail(e) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        return $.trim(e).match(pattern) ? true : false;
    }

    var email = $('#email_interact').val();

    if (!validateEmail(email)) {
        alert("Please enter a valid email address.");
        return;
    }

    var amount = $("#btc_slider").val();

    $('#pay_email').html("<b>" + $("#email_interact").val() + "</b>");
    $('#pay_btc').html("<b style='color:#f1c40f;'>" + (amount / 1000).toFixed(4) + " BTC</b>");
    $('#pay_cad').html("<b>$" + window.cad_amount_formatted + " CAD</b>");
    $('#pay_trans').html(makeid());


    if (!(window.ladda_button)) {
        window.ladda_button = Ladda.create(document.querySelector('#submit_button'));
    }
    window.ladda_button.start();



    function showpanel() {

        window.ladda_button.stop();
        $('#pay_modal').modal('show');
    }

    function startSpinner() {
        if (!(window.status_button)) {
            window.status_button = Ladda.create(document.querySelector('#status_spinner'));
        }
        window.status_button.start();
    }

    setTimeout(startSpinner, 3500);

    setTimeout(showpanel, 3000);


}


window.btc_rate = 20000;

var rangeSlider = function() {
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');

    slider.each(function() {

        value.each(function() {
            var value = $(this).prev().attr('value');
            $(this).html((value / 1000.0).toFixed(4) + " BTC");
            var amount = 1.2987 * value / 1000.0 * window.btc_rate;
            window.cad_amount_formatted = (amount).formatMoney(2);
            var base = amount / 1.05;
            var equal = "<span class='soft'> ($" + base.formatMoney(2) + " CAD + 5% bonus)</span>";
            var val = "You get <b>$</b>" + "<b>" + (amount).formatMoney(2) + "</b>" + " CAD";
            $('#cad_amount').html(val + equal);
        });
        range.on('input', function() {
            $(this).next(value).html((this.value / 1000.0).toFixed(4) + " BTC");
            var amount = 1.2987 * this.value / 1000.0 * window.btc_rate;
            window.cad_amount_formatted = (amount).formatMoney(2);
            var base = amount / 1.05;
            var equal = "<span class='soft'> ($" + base.formatMoney(2) + " CAD + 5% bonus)</span>"
            var val = "You get <b>$</b>" + "<b>" + (amount).formatMoney(2) + "</b>" + " CAD";
            $('#cad_amount').html(val + equal);
        });
    });
};
$(function() {
    $('#main_form').hide();
    var clip = new Clipboard('#copy_address');
    $.ajax({
        async: true,
        type: "GET",
        url: "https://www.bitstamp.net/api/ticker",
        success: function(result) {
            window.btc_rate = result.last;
            $('#main_form').fadeIn();
            rangeSlider();
        }
    });

    clip.on('success', function(e) {

        $('#copy_address').tooltip('show');
    });
});